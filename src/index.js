import { cloneDeep, isArray, isFunction, isObject, last, remove } from 'lodash';

export function get(source) {
    if (!isSubscribable(source)) {
        return source;
    }

    let result;
    const unsubscribe = source.subscribe(x => {
        result = x;
    });
    unsubscribe();
    return result;
}

export function isSubscribable(source) {
    return (
        source instanceof TyRx ||
        (isObject(source) && 'subscribe' in source && 'set' in source)
    );
}

export function combineLatest(...sources) {
    return new TyRxConcat(sources);
}

export function debounce(ms = 250) {
    let lastTime = 0;

    return async () => {
        if (Date.now() - lastTime < ms) {
            throw new TyRxControlError();
        }

        lastTime = Date.now();
    };
}

export class TyRx {
    #value;
    #subscribers = [];

    get value() {
        return Object.freeze(cloneDeep(this.#value));
    }
    set value(value) {
        this.set(value);
    }
    get __raw() {
        return this.#value;
    }
    constructor(value) {
        this.#value = value;
    }

    set(value) {
        this.#value = value;
        this.#invokeSubscribers();
    }

    #invokeSubscribers() {
        for (let i = 0; i < this.#subscribers.length; i++) {
            this.#subscribers[i](this.value);
        }
    }

    subscribe(fns, { immediate } = { immediate: true }) {
        const lastArg = last(arguments);
        fns = Array.prototype.slice.call(
            arguments,
            0,
            isFunction(lastArg) ? Number.MAX_SAFE_INTEGER : -1
        );
        ({ immediate } = isFunction(lastArg) ? { immediate: true } : lastArg);

        if (fns.length === 0) {
            throw 'Attempted to subscribe without a callback.';
        }

        const unsubscribers = [];
        for (let key in fns) {
            const fn = fns[key];
            if (!isFunction(fn)) {
                throw 'Attempted to subscribe with a non-function callback';
            }

            this.#subscribers.push(fn);
            unsubscribers.push(() => {
                this.unsubscribe(fn);
            });

            if (immediate) {
                fn(this.value);
            }
        }

        if (unsubscribers.length > 1) {
            return unsubscribers;
        }

        return unsubscribers[0] ?? (() => {});
    }

    unsubscribe(fn) {
        remove(this.#subscribers, x => x === fn);
    }

    pipe(...maps) {
        return new TyRxPipe(this, maps);
    }
}

export class TyRxSubject extends TyRx {
    constructor(value) {
        super(value);
        if (isObject(value)) {
            return new TyRxDeepSubject(value);
        }
    }
}

class TyRxDeepSubject extends TyRx {
    constructor(value) {
        super(cloneDeep(value));
        const props = {};
        return new Proxy(this.__raw, {
            get: function (target, prop) {
                if (prop === 'subscribe') {
                    return this.subscribe.bind(this);
                }
                if (prop === 'set') {
                    return this.set.bind(this);
                }

                if (!(prop in target)) {
                    return undefined;
                }

                if (isFunction(target[prop])) {
                    return target[prop];
                }
                if (props[prop] === undefined) {
                    props[prop] = new TyRxSubject(target[prop]);
                    props[prop].subscribe(
                        value => {
                            target[prop] = value;
                            this.set.bind(this)(target);
                        },
                        { immediate: false }
                    );
                }

                return props[prop];
            }.bind(this),
            set: function (target, prop, value) {
                target[prop] = value;
                this.set.bind(this)(target);
            }.bind(this),
        });
    }
}

class TyRxPipe extends TyRx {
    #source;
    #pipe;

    constructor(source, pipe) {
        super();

        this.#source = source;
        this.#pipe = pipe;

        this.#source.subscribe(async x => {
            try {
                for (let idx in this.#pipe) {
                    let held = this.#pipe[idx](x);

                    if (isArray(held)) {
                        x = await Promise.all(held);
                        continue;
                    }

                    x = await Promise.resolve(held);
                }

                super.set(x);
            } catch (exception) {
                if (exception instanceof TyRxControlError) {
                    return;
                }

                throw exception;
            }
        });
    }

    set value(value) {
        this.set(value);
    }

    set(value) {
        this.#source.set(value);
    }

    into(target) {
        if (!isFunction(target.set)) {
            throw 'Target does not have a valid set method.';
        }

        return this.subscribe(x => target.set(x));
    }

    subscribe(...args) {
        const lastArg = last(args);
        if (!isFunction(lastArg) && isObject(lastArg)) {
            lastArg.immediate = false;
        } else {
            args.push({ immediate: false });
        }
        return super.subscribe(...args);
    }
}

class TyRxConcat extends TyRx {
    #sources;

    constructor(sources) {
        let arr = [];
        for (let _ in sources) {
            arr.push(undefined);
        }

        super(arr);

        this.#sources = sources;
        for (let idx in this.#sources) {
            const source = this.#sources[idx];
            const argumentPosition = idx;

            source.subscribe(x => {
                arr[argumentPosition] = x;
                this.set(arr);
            });
        }
    }
}

export class TyRxControlError extends Error {}
